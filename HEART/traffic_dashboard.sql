/*
GA dataset names for 
ios - 118341991
web - 108795712
android - 118356700
*/



-- Web, mWeb base view: This is the query for view dbdev.v_atlas_traffic_web which fetches 
-- the base data for Web and mWeb data of users. A query would be written on top of this 
-- view (data) along with other Platforms to obtain the metrics for traffic, with filters
--  on fields 'Logged In', Date range, Network Type (2G/ 3G etc.) etc.
SELECT STRING(VisitId) as VisitId,
       fullVisitorId,
       CONCAT(STRING(VisitId), fullVisitorId) as Session_ID,
       visitStartTime,
       COALESCE(device.deviceCategory, 'Unknown-Web') AS Device_Category, 
       CASE WHEN UPPER(device.deviceCategory) IN ('MOBILE', 'TABLET') THEN 'mWeb' 
            WHEN UPPER(device.deviceCategory) = 'DESKTOP' THEN 'Web' 
            ELSE 'Unknown-Web'
       END AS Platform_Type,
       Case when trafficSource.source = '(direct)' and trafficSource.medium =  '(none)' then 'Direct' 
           when trafficSource.medium = 'organic' then 'organic' 
           else 'Paid' END as Search_Type, 
       hits.appInfo.appVersion as App_Version,
       geoNetwork.region as Region, 
--       geoNetwork.metro as Metro
        CASE WHEN FIRST(IF(customDimensions.index=7, customDimensions.value, NULL)) IS NULL THEN 'NOT LOGGED IN'
             ELSE 'LOGGED IN' END as Logged_In, 
         COALESCE(FIRST(IF(customDimensions.index=53, UPPER(customDimensions.value), NULL)), 'Not Applicable') AS Network_type
FROM FLATTEN(
(SELECT VisitId,  fullVisitorId, visitStartTime, device.deviceCategory
        , trafficSource.source, trafficSource.medium, hits.appInfo.appVersion, geoNetwork.region
        , customDimensions.index,  customDimensions.value
FROM TABLE_DATE_RANGE([108795712.ga_sessions_], TIMESTAMP('2016-04-01'), TIMESTAMP (current_date()))
), hits) A
GROUP BY VisitId, fullVisitorId, Session_ID, 
       visitStartTime, Device_Category, Platform_Type,
       Search_Type, 
       App_Version, Region


-- Android base view: This is the query for view dbdev.v_atlas_traffic_android which fetches 
-- the base data for Android data of users. A query would be written on top of this 
-- view (data) along with other Platforms to obtain the metrics for traffic, with filters
--  on fields 'Logged In', Date range, Network Type (2G/ 3G etc.) etc.
SELECT STRING(VisitId) as VisitId,
       fullVisitorId,
       CONCAT(STRING(VisitId), fullVisitorId) as Session_ID,
       visitStartTime,
       COALESCE(device.deviceCategory, 'Unknown-ANDROID') AS Device_Category, 
       'Android' AS Platform_Type,
       Case when trafficSource.source = '(direct)' and trafficSource.medium =  '(none)' then 'Direct' 
           when trafficSource.medium = 'organic' then 'organic' 
           else 'Paid' END as Search_Type, 
       hits.appInfo.appVersion as App_Version,
       geoNetwork.region as Region, 
--       geoNetwork.metro as Metro
        CASE WHEN FIRST(IF(customDimensions.index=7, customDimensions.value, NULL)) IS NULL THEN 'NOT LOGGED IN'
             ELSE 'LOGGED IN' END as Logged_In, 
         FIRST(IF(customDimensions.index=53, customDimensions.value, NULL)) Network_type
FROM FLATTEN(
(SELECT VisitId,  fullVisitorId, visitStartTime, device.deviceCategory
        , trafficSource.source, trafficSource.medium, hits.appInfo.appVersion, geoNetwork.region
        , customDimensions.index,  customDimensions.value
FROM TABLE_DATE_RANGE([118356700.ga_sessions_], TIMESTAMP('2016-04-01'), TIMESTAMP (current_date()))
), hits) A
GROUP BY VisitId, fullVisitorId, Session_ID, 
       visitStartTime, Device_Category, Platform_Type,
       Search_Type, 
       App_Version, Region




-- iOS base view: This is the query for view dbdev.v_atlas_traffic_ios which fetches 
-- the base data for iOS data of users. A query would be written on top of this 
-- view (data) along with other Platforms to obtain the metrics for traffic, with filters
--  on fields 'Logged In', Date range, Network Type (2G/ 3G etc.) etc.
SELECT STRING(VisitId) as VisitId,
       fullVisitorId,
       CONCAT(STRING(VisitId), fullVisitorId) as Session_ID,
       visitStartTime,
       COALESCE(device.deviceCategory, 'Unknown-iOS') AS Device_Category, 
       'iOS' AS Platform_Type,
       Case when trafficSource.source = '(direct)' and trafficSource.medium =  '(none)' then 'Direct' 
           when trafficSource.medium = 'organic' then 'organic' 
           else 'Paid' END as Search_Type, 
       hits.appInfo.appVersion as App_Version,
       geoNetwork.region as Region, 
--       geoNetwork.metro as Metro
        CASE WHEN FIRST(IF(customDimensions.index=7, customDimensions.value, NULL)) IS NULL THEN 'NOT LOGGED IN'
             ELSE 'LOGGED IN' END as Logged_In, 
         COALESCE(FIRST(IF(customDimensions.index=53, UPPER(customDimensions.value), NULL)), 'Unavailable') AS Network_type
FROM FLATTEN(
(SELECT VisitId,  fullVisitorId, visitStartTime, device.deviceCategory
        , trafficSource.source, trafficSource.medium, hits.appInfo.appVersion, geoNetwork.region
        , customDimensions.index,  customDimensions.value
FROM TABLE_DATE_RANGE([118341991.ga_sessions_], TIMESTAMP('2016-04-01'), TIMESTAMP (current_date()))
), hits) A
GROUP BY VisitId, fullVisitorId, Session_ID, 
       visitStartTime, Device_Category, Platform_Type,
       Search_Type, 
       App_Version, Region


-- Query which runs on the views [dbdev.v_atlas_traffic_android], [dbdev.v_atlas_traffic_ios] and 
-- [dbdev.v_atlas_traffic_web]  and fetches results. Users may manually add/ edit the values for filters
-- (fields/ columns) in WHERE condition as per their requirement.
SELECT CASE WHEN Platform_Group = 1 THEN 'All'
            ELSE Platform_Type END AS Platform_Type,
       Direct,
       Organic,
       Paid,
       Total
FROM(
SELECT COALESCE(Platform_Type, 'Unknown') as Platform_Type,
       GROUPING(Platform_Type) as Platform_Group,
      ROUND((SUM(CASE WHEN Search_Type = 'DIRECT' THEN INTEGER(CNR) ELSE INTEGER(0) END)/ SUM(CNR)) * 100, 3)  as Direct,
      ROUND((SUM(CASE WHEN Search_Type = 'ORGANIC' THEN INTEGER(CNR) ELSE INTEGER(0) END)/ SUM(CNR)) * 100, 3) as Organic,
      ROUND((SUM(CASE WHEN Search_Type = 'PAID' THEN INTEGER(CNR) ELSE INTEGER(0) END)/ SUM(CNR)) * 100, 3) as Paid,
     SUM (CNR) as Total
FROM (
SELECT DATE(dt.date )  as Session_Date, weekdayname as Weekday_Name, 
       DATE(Week_Start_Date) as Week_Start_Date,
       EXACT_COUNT_DISTINCT(base.Session_ID) as CNR,
       Platform_Type,
       UPPER(Search_Type) as Search_Type,
       UPPER(Network_type) as Network_type,
       App_Version,
       Region, 
       Logged_In
FROM dbdev.date_dimension dt
INNER JOIN (SELECT *, 
                   (visitStartTime * 1000) - ((visitStartTime * 1000) % 86400000) as epoch_start_day
            FROM [dbdev.v_atlas_traffic_android] droid,
                 [dbdev.v_atlas_traffic_ios] ios,
                 [dbdev.v_atlas_traffic_web] web) base
      ON base.epoch_start_day = dt.epoch_gmt
WHERE DATE(dt.date) between DATE('2016-04-01') AND DATE('2016-04-14')
-- AND Logged_In IN ('NOT LOGGED IN', 'LOGGED IN')
-- AND Network_type IN ('2G', '3G', 'WIFI')
GROUP BY Session_Date, 
         Weekday_Name,
         Week_Start_Date,
          Platform_Type,
       Search_Type,
       Network_type,
       App_Version,
       Region,
       Logged_In
       

       ) base_2

GROUP BY ROLLUP(Platform_Type)
) sheath
ORDER BY 1

