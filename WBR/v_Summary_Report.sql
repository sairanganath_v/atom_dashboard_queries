-- v_Summary_Report pulling categories from deal : matching with Shadow
-- Addded Timezone correction in both filter and Week DAte.

SELECT Category,  Week_start_date, Metric_Name, ROUND(MEtric_Value, 2) as Metric_Value
FROM 
(SELECT -- deal.Category as Category, 
         COALESCE(deal.Category, ol.categoryid ) as Category, 
--              DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((oh.createdat*1000 + 19800000), 1), '%Y-%m-%d')) as Week_start_date,
              DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((oh.createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'Gross Bookings (Absolute)' Metric_Name, 
           SUM( ol.unitprice )/100 as Metric_Value
      FROM  ( SELECT orderid, createdat, totalprice, payable, source, ispaid FROM [Atom.order_header])  oh
          INNER JOIN (SELECT orderid, categoryid, unitprice, dealid 
                      FROM [Atom.order_line]
                      GROUP BY orderid, categoryid, unitprice, dealid) ol
             ON oh.orderid = ol.orderid
          INNER JOIN (SELECT STRING(_id) as dealid, dCat.id as Category FROM [Atom.deal] ) deal
             ON deal.dealid = ol.dealid
      WHERE oh.ispaid = 't'
         AND source is not null
         and source <> 'mobile'
--         and DATE(MSEC_TO_TIMESTAMP( oh.createdat - 19800000 )) > DATE('2015-08-31') -- Data from Sept 2015 and upto Last week
         -- and DATE(MSEC_TO_TIMESTAMP( oh.createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                  END -- before_Date
      GROUP BY Category, Week_start_date, Metric_Name
) gb_abs
,
(SELECT Category, 
        Week_Start_Date as Week_start_date, 
        'Gross Revenue (%)' Metric_Name, 
        (FLOAT(MEtric_Value/ All_Value) * 100) as Metric_value
FROM ( SELECT Category, Week_Start_Date, SUM(MEtric_Value) as MEtric_Value, 
 SUM(MEtric_Value) OVER (PARTITION BY Week_Start_Date) as All_Value
 FROM (SELECT COALESCE(deal.Category, ol.categoryid ) as Category, 
          DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((oh.createdat*1000 + 19800000000), 1), '%Y-%m-%d'))as Week_Start_Date,
          SUM(case when marginpercentage is null then cast(flatcommission as float)/100 
                   else cast(unitprice as float)/100*cast(marginpercentage as float)/100 
              end) as Metric_Value
      FROM  ( SELECT orderid, createdat, totalprice, payable, source, ispaid FROM [Atom.order_header])  oh
           INNER JOIN (SELECT orderid, categoryid, dealid, ispaid, 
                              marginpercentage, flatcommission, unitprice 
                       FROM [Atom.order_line]
                       GROUP BY orderid, categoryid, dealid, ispaid, 
                              marginpercentage, flatcommission, unitprice) ol
             ON ol.orderid = oh.orderid
           INNER JOIN (SELECT STRING(_id) as dealid, dCat.id as Category FROM [Atom.deal] ) deal
             ON deal.dealid = ol.dealid
      WHERE oh.ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP( oh.createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                  END -- before_Date
      GROUP BY Week_Start_Date, Category
)
 GROUP BY Category, Week_Start_Date
 )) GR_pc
 ,
 (SELECT Category,  Week_start_date, 
        'Net Revenue (%)' as Metric_Name,
       (FLOAT(MEtric_Value/all_Metric_Value) * 100) as Metric_Value
FROM (
  SELECT Category, Week_start_date, SUM(MEtric_Value) as MEtric_Value, 
         SUM(MEtric_Value) OVER (PARTITION BY Week_start_date) as all_Metric_Value
  FROM (
        SELECT  COALESCE(deal.Category, ol.categoryid ) as Category, 
               DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((oh.createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
              SUM(oh.totalprice -  payable) as MEtric_Value
        FROM [Atom.order_header] oh
        INNER JOIN   (SELECT orderid, dealid, categoryid FROM [Atom.order_line] GROUP BY orderid, categoryid, dealid) ol
                  ON oh.orderid = ol.orderid
        INNER JOIN (SELECT STRING(_id) as dealid, dCat.id as Category FROM [Atom.deal] ) deal
             ON deal.dealid = ol.dealid          
        WHERE oh.ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         --and DATE(MSEC_TO_TIMESTAMP( oh.createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
         --                                                            ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
         --                                                        END -- before_Date
        GROUP BY Week_start_date, Category
  ) 
  GROUP BY  Category, Week_start_date
)) NR_pc

ORDER BY Week_start_date desc, Category