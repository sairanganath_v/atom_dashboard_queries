-- v_Platform_Report_v2
-- Includes timezone handling and matches with Shadow.
SELECT source, 
       Week_Start_Date  Week_Start_Date,
       Metric_Name,
       ROUND(Metric_Value, 2) as Metric_Value
FROM 
(SELECT source, 
           -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date,
           DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'Gross Billings (Absolute)' Metric_Name, 
           SUM(totalprice)/100 as Metric_Value
      FROM [Atom.order_header] 
      WHERE ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP(createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date 
      GROUP BY source, Week_start_date, Metric_Name
      
) GB_abs,
(SELECT oh.source as source, 
          -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(oh.createdat), 1), '%Y-%m-%d') as Week_Start_Date,
           DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((oh.createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'Gross Revenue (Absolute)' Metric_Name, 
--            ROUND(SUM(totalprice)/100, 2) as Metric_Value
SUM(case when marginpercentage is null then cast(flatcommission as float)/100 else
cast(unitprice as float)/100*cast(marginpercentage as float)/100 end) as Metric_Value
      FROM [Atom.order_header] oh 
           INNER JOIN [Atom.order_line] ol
             ON ol.orderid = oh.orderid
      WHERE oh.ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP( oh.createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      GROUP BY source, Week_start_date, Metric_Name
) GR_abs,

 (SELECT source, 
        Week_Start_Date, '% Share of GB' Metric_Name, 
        FLOAT((Gross_Billings/all_platform_GB) * 100)  as Metric_Value

 FROM (
SELECT  Week_Start_Date, source, Gross_Billings,  SUM(Gross_Billings) OVER (PARTITION BY Week_Start_Date ) as all_platform_GB
FROM (SELECT source, 
          --  STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date,
          DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'Gross Billings (Absolute)' Metric_Name, 
           SUM(totalprice) as Gross_Billings
      FROM [Atom.order_header] 
      WHERE ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP(createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
            
      GROUP BY source, Week_Start_Date, Metric_Name)
 )
 ) GB_pc,
 
 (
SELECT CASE WHEN rollup_source = 1 THEN 'ALL' ELSE source END as source,
       Week_Start_Date,
       'No. of Transactions' as Metric_Name, 
       FLOAT(Metric_Value) as Metric_Value
FROM (SELECT source, GROUPING(source) as rollup_source,
       -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date,  
          DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
       Count(3) as Metric_Value
FROM [Atom.order_header] 
WHERE ispaid = 't'
  AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP(createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      
GROUP BY ROLLUP(Week_Start_Date, source)
)) txn_CNR,


(SELECT CASE WHEN rollup_source = 1 THEN 'ALL' ELSE source END as source,
       Week_Start_Date,
       'No. of Transactions per Customer' as Metric_Name, 
       ROUND(FLOAT(Metric_Value), 2) as Metric_Value
FROM (
SELECT source, GROUPING(source) as rollup_source,
       -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date, 
        DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
       ROUND(FLOAT(Count(3) /count(distinct customerid)), 2) as Metric_Value       
FROM [Atom.order_header] 
WHERE ispaid = 't'
  AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP(createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
--       )
 GROUP BY ROLLUP(Week_start_date, source)
) ) txn_per_Cust_CNR,

(SELECT source, 
           DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'AOV' Metric_Name, 
           ROUND(FLOAT((SUM(totalprice)/100)/ count(3)), 2) as Metric_Value 
      FROM [Atom.order_header] 
      WHERE ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP(createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      GROUP BY source, Week_start_date, Metric_Name) as AOV
 order by Week_Start_Date desc, Metric_Name, source