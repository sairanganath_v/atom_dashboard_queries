-- v_Others_Summary_Report
SELECT DATE(Week_Start_Date) as Week_Start_Date, Metric_Name, ROUND(Metric_Value, 2) as Metric_Value
FROM 
-- Total no of transactions
(SELECT  
       -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date,  
       DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
              'No. of Transactions' as Metric_Name, 
       FLOAT(Count(3)) as Metric_Value
FROM [Atom.order_header] 
WHERE ispaid = 't'
  AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      
GROUP BY Week_Start_Date) txn_CNR_All
,
-- AOV
(SELECT    -- STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d') as Week_Start_Date,
           DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
          'AOV' Metric_Name, 
           FLOAT((SUM(totalprice)/100)/ count(3)) as Metric_Value 
      FROM [Atom.order_header] 
      WHERE ispaid = 't'
         AND source is not null
         and source <> 'mobile'
         -- and DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      
      GROUP BY Week_Start_Date, Metric_Name) AOV_All
,
-- Mobile Share
(


SELECT Week_Start_Date, 
          'Mobile Share' Metric_Name, 
       FLOAT(SUM(CASE WHEN source IN ('app_android', 'app_ios') THEN Metric_Value * 100 ELSE 0 END ) / All_GB) as Metric_Value
FROM (
SELECT Week_Start_Date, source, Metric_Value, SUM (Metric_Value) OVER (PARTITION BY Week_Start_Date) as All_GB
FROM (
SELECT DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
           source,
        SUM(totalprice) as Metric_Value
      FROM [Atom.order_header] 
      WHERE ispaid = 't'
         -- and DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
      GROUP BY Week_Start_Date,  source)
      )
Group by Week_Start_Date, All_GB) GB_Mob_Shr
,
-- New Customers
(SELECT Week_Start_Date, 
       'New Customers' Metric_Name,
       FLOAT(Sum(is_New)) as Metric_Value
FROM (
SELECT base2.CustomerID as Customer,  wee.Week_Start_Date as Week_Start_Date, 
       CASE WHEN wee.Week_Start_Date = base2.Week_Start_Date THEN 1 ELSE 0 END as is_New,
       CASE WHEN wee.Week_Start_Date > base2.Week_Start_Date THEN 1 ELSE 0 END as is_Existing
FROM 
(
SELECT -- DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d')) as Week_Start_Date
       DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK((createdat*1000 + 19800000000), 1), '%Y-%m-%d')) as Week_start_date,
FROM [Atom.order_header]
-- WHERE DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY Week_Start_Date) wee

CROSS JOIN 

(SELECT CustomerID, 
       DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(MIN(createdat)*1000), 1), '%Y-%m-%d')) as Week_Start_Date
FROM [Atom.customer] cust 
-- WHERE DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY CustomerID
) base2
)
GROUP BY Week_Start_Date
) New_Customers
,
-- Existing Customers
(SELECT Week_Start_Date, 
       'Existing Customers' Metric_Name,
       FLOAT(SUM(is_Existing)) as Metric_Value
FROM (
SELECT base2.CustomerID as Customer,  wee.Week_Start_Date as Week_Start_Date, 
       CASE WHEN wee.Week_Start_Date = base2.Week_Start_Date THEN 1 ELSE 0 END as is_New,
       CASE WHEN wee.Week_Start_Date > base2.Week_Start_Date THEN 1 ELSE 0 END as is_Existing
FROM 
(
SELECT DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(createdat), 1), '%Y-%m-%d')) as Week_Start_Date
FROM [Atom.order_header]
-- WHERE  DATE(MSEC_TO_TIMESTAMP( createdat + 19800000 )) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date 
GROUP BY Week_Start_Date) wee

CROSS JOIN 

(SELECT CustomerID, 
       DATE(STRFTIME_UTC_USEC(UTC_USEC_TO_WEEK(MSEC_TO_TIMESTAMP(MIN(createdat)*1000), 1), '%Y-%m-%d')) as Week_Start_Date
FROM [Atom.customer] cust 
GROUP BY CustomerID
) base2
)
GROUP BY Week_Start_Date
) Old_Customers
ORDER BY Week_Start_Date desc, Metric_Name