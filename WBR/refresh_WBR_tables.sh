#Date: 03 May 2016
#Script Name: refresh_WBR_tables.sh
#Script Purpose: To refresh the source tables of WBR as views are failing via tableau (for being expensive).
#Script Location: ubuntu@db-db-10-2-4-25:/home/ubuntu/ETL/WBR/
#Author: Sairanganath Vemuri
#Comments/ History:
#
#(dd/mon/yyyy)| <Developer_name> |                       Comments
#--------------------------------------------------------------------------------------------------
# 09/May/2016 | Sairanganath . V | Corrected Week_Start_Date calculation for tbl_GB_Category Query and rectified Category 
#                                  consideration in view dbdev.Prive_Value. Added email statement for notifications.



#send email stating process has started
mail -s "WBR Source Tables refresh process started at: `date`"  sairanganath.v@nearbuy.com  < /dev/null

echo "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
echo "WBR source tables refresh started at  `date`"

#Summary Report - 2 Queries
bq query --maximum_billing_tier 100 --replace --destination_table=dbdev.tbl_Summary_Report "SELECT * FROM dbdev.v_Summary_Report" &
summ=$!
bq query --maximum_billing_tier 100 --replace --destination_table=dbdev.tbl_Others_Summary_Report "SELECT * FROM dbdev.v_Others_Summary_Report" &
summ_oth=$!

# Platform Report
bq query --maximum_billing_tier 100 --replace --destination_table=dbdev.tbl_Platform_Report "SELECT * FROM dbdev.v_Platform_Report" &
plat=$!

# Category Report - 2 Queries
bq query --maximum_billing_tier 100 --replace --destination_table=dbdev.tbl_Counts_Category_Report_2 "SELECT * FROM dbdev.v_Counts_Categorywise_Report_2" &
catg_cnr=$!

bq query --maximum_billing_tier 100 --replace --destination_table=dbdev.tbl_GB_Category "select gr, gb, promo, nr, Category_ID, orderid, takerate, D.DATE as createdate, 
      Week_Start_Date, Week_End_Date , Cancellation_Amount, Refund_Amount, AOV, APP_AOV, Desktop_AOV, Mobile_AOV 
FROM  (SELECT gr, gb, promo, nr, createdAt, (createdAt -  createdAt % 86400000 - 19800000) as  createdAt_ist, 
		       categoryId as Category_ID, takerate,orderid, Cancellation_Amount, Refund_Amount, 
		       AOV, APP_AOV, Desktop_AOV, Mobile_AOV   
	   FROM [dbdev.Price_Value]
	   )base 
  INNER JOIN (SELECT epoch_ist, DATE(Week_Start_Date) as Week_Start_Date, 
                     DATE(Week_End_Date) as Week_End_Date, DATE(DATE) as DATE
  	          FROM   dbdev.date_dimension
  	         ) D             
    on D.epoch_ist = base.createdAt_ist            
   WHERE DATE(D.Week_Start_Date) BETWEEN DATE('2015-10-01') AND CURRENT_DATE()
-- WHERE DATE(D.Week_Start_Date) BETWEEN DATE('2015-10-01') AND CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                      ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                  END 
order by Week_Start_Date desc" &
catg_gb=$!

echo "Waiting for all the table load process to complete: `date`"



wait $summ $summ_oth $plat $catg_cnr $catg_gb

echo "WBR source tables refresh ended at `date`"
echo "   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
mail -s "WBR Source Tables refresh process Ended at: `date`"  sairanganath.v@nearbuy.com  < /dev/null

