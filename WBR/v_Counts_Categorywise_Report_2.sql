-- dbdev.v_Counts_Categorywise_Report_2
SELECT * FROM 
-- Deals Submitted
(
SELECT -- DATE(dt.DATE) as Date,  
        DATE(dt.Week_Start_Date) as Week_Start_Date,
        DATE(dt.Week_End_Date) as Week_End_Date,
        'No. of Deals Submitted' as Metric_Name,
        COALESCE(deal.Category, 'Unknown') as Category,
       COALESCE(INTEGER(COUNT(deal.deal_ID)), INTEGER(0) ) as Metric_Value
FROM [dbdev.date_dimension] dt
LEFT JOIN (SELECT deal._id as Deal_ID, 
                 deal.units.dval.dFrmDt, deal.units.dval.dToDt,
                 COALESCE(deal.createdTime, deal.updateHistory.createdAt ) as CreatedAt,
                 COALESCE(deal.createdTime, deal.updateHistory.createdAt) -  COALESCE(deal.createdTime, deal.updateHistory.createdAt) % 86400000 - 19800000 as CreatedAt_Date,
                 COALESCE(deal.dCat.id, deal.categoryId) as Category,
                 deal.updateHistory.submittedAt as SubmittedAt,
                 deal.updateHistory.submittedAt -  updateHistory.submittedAt % 86400000 - 19800000 as SubmittedAt_Date,
                 deal.state as Deal_State,
                 COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt) as madeLiveAt,
                 COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt) - COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt)% 86400000 - 19800000  as madeLiveAt_Date

          FROM [Atom.deal] deal ) deal
          ON dt.epoch_ist = SubmittedAt_Date
WHERE DATE(dt.DATE) < CURRENT_DATE()
-- WHERE DATE(dt.DATE) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY -- Date, 
         Week_Start_Date, Week_End_Date, Category
) deals_Sub
,
-- Deals Made Live 
(
SELECT  -- DATE(dt.DATE) as Date,  
        DATE(dt.Week_Start_Date) as Week_Start_Date,
        DATE(dt.Week_End_Date) as Week_End_Date,
        'No. of Deals Made Live' as Metric_Name,
        COALESCE(deal.Category, 'Unknown') as Category,
       COALESCE(INTEGER(COUNT(deal.deal_ID)), INTEGER(0) ) as Metric_Value
FROM [dbdev.date_dimension] dt
LEFT JOIN (SELECT deal._id as Deal_ID, 
                 deal.units.dval.dFrmDt, deal.units.dval.dToDt,
                 COALESCE(deal.createdTime, deal.updateHistory.createdAt ) as CreatedAt,
                 COALESCE(deal.createdTime, deal.updateHistory.createdAt) -  COALESCE(deal.createdTime, deal.updateHistory.createdAt) % 86400000 - 19800000 as CreatedAt_Date,
                 COALESCE(deal.dCat.id, deal.categoryId) as Category,
                 deal.updateHistory.submittedAt as SubmittedAt,
                 deal.updateHistory.submittedAt -  updateHistory.submittedAt % 86400000 - 19800000 as SubmittedAt_Date,
                 deal.state as Deal_State,
                 COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt) as madeLiveAt,
                 COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt) - COALESCE(deal.madeLiveAt, deal.updateHistory.madeLiveAt)% 86400000 - 19800000  as madeLiveAt_Date

          FROM [Atom.deal] deal ) deal
          ON dt.epoch_ist = madeLiveAt_Date
WHERE DATE(dt.DATE) < CURRENT_DATE()
-- WHERE DATE(dt.DATE) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY -- Date, 
         Week_Start_Date, Week_End_Date, Category
) deals_made_Live
,
(SELECT -- DATE(dt.DATE) as Date,  
        DATE(dt.Week_Start_Date) as Week_Start_Date,
        DATE(dt.Week_End_Date) as Week_End_Date,
        'No. of Transactions' as Metric_Name,
        COALESCE(oh_Cat.Category, 'Unknown') as Category,
       COALESCE(INTEGER(COUNT(oh_Cat.Order_ID)), INTEGER(0) ) as Metric_Value
FROM [dbdev.date_dimension] dt
LEFT JOIN (SELECT  oh.orderid as Order_ID, 
               oh.createdat as Order_CreatedAt,  
               oh.createdat -  oh.createdat % 86400000 - 19800000 as Order_CreatedAt_Date,
               deal.Category as Category
          FROM (SELECT STRING(_id) as Deal_ID,  COALESCE(dCat.id, categoryId) as Category FROM [Atom.deal] ) deal
                INNER JOIN [Atom.order_line] ol 
                         ON deal.Deal_ID = ol.dealid
               INNER JOIN [Atom.order_header] oh    
                  ON oh.orderid = ol.orderid
          WHERE oh.ispaid = 't'
            AND oh.source is not null
                   and oh.source <> 'mobile'      
          GROUP BY Order_ID, Order_CreatedAt, Category, Order_CreatedAt_Date
) oh_Cat
ON dt.epoch_ist = oh_Cat.Order_CreatedAt_Date
WHERE DATE(dt.DATE) < CURRENT_DATE()
-- WHERE DATE(dt.DATE) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY -- Date, 
         Week_Start_Date, Week_End_Date, Category
) txn_CNR
,

(
SELECT -- DATE(dt.DATE) as Date,  
        DATE(dt.Week_Start_Date) as Week_Start_Date,
        DATE(dt.Week_End_Date) as Week_End_Date,
        'No. of New Transacting Customers' as Metric_Name,
        COALESCE(oh_Cat.Category, 'Unknown') as Category,
        INTEGER(Count(Customer_ID)) as Metric_Value
FROM [dbdev.date_dimension] dt
LEFT JOIN (

SELECT cust.customerId as Customer_ID, -- cust.orderSummary.firstPurchaseDetail.purchaseDate as first_purch_dt, 
       cust.orderSummary.firstPurchaseDetail.orderId first_order_id
      , oh.source as Source, ol.dealid as Deal_ID, deal.Category as Category,
      oh.createdat as createdat, 
      oh.createdat -  oh.createdat % 86400000 as Order_CreatedAt_Date
FROM [Atom.customer] cust
    INNER JOIN [Atom.order_header] oh ON oh.orderid = cust.orderSummary.firstPurchaseDetail.orderId aND cust.customerid = oh.customerid
    INNER JOIN [Atom.order_line] ol ON ol.orderid = oh.orderid
    INNER JOIN (SELECT STRING(_id) as Deal_ID,  COALESCE(dCat.id, categoryId) as Category FROM [Atom.deal] ) deal ON deal.Deal_ID = ol.dealid
WHERE oh.ispaid = 't'
            AND ol.ispaid = 't'
            AND oh.source is not null
                   and oh.source <> 'mobile'  
       GROUP BY Customer_ID, first_order_id, Source, Deal_ID, Category, createdat, Order_CreatedAt_Date -- to eliminate duplicates from Order_line JOIN

) oh_Cat
ON dt.epoch_gmt = oh_Cat.Order_CreatedAt_Date
WHERE DATE(dt.DATE) < CURRENT_DATE()
-- WHERE DATE(dt.DATE) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY -- Date, 
         Week_Start_Date, Week_End_Date, Category
ORDER BY  Week_Start_Date desc, Category
) new_Cust
,
(SELECT DATE(dt.Week_Start_Date) as Week_Start_Date,
        DATE(dt.Week_End_Date) as Week_End_Date,
        'No. of Existing Transacting Customers' as Metric_Name,
        COALESCE(base3.Category, 'Unknown') as Category,
        INTEGER(base3.Existing_Customers)  as Metric_Value
FROM [dbdev.date_dimension] dt
LEFT JOIN 
(
SELECT Order_CreatedAt_WeekDate, Category, COUNT(Customer_ID) as Existing_Customers
FROM (SELECT Order_CreatedAt_WeekDate,  Category, is_New_Customer, Customer_ID
FROM 
(SELECT  oh.orderid as Order_ID, 
               oh.createdat as Order_CreatedAt,
               oh.createdat -  oh.createdat % 86400000 - 19800000 as Order_CreatedAt_WeekDate,
               deal.Category as Category,
               cust.customerId as Customer_ID,
               CASE WHEN COALESCE(cust.First_Order_ID, 0) = oh.orderid  THEN 1 ELSE 0 END  as is_New_Customer
          FROM (SELECT STRING(_id) as Deal_ID,  COALESCE(dCat.id, categoryId) as Category FROM [Atom.deal] ) deal
                INNER JOIN [Atom.order_line] ol 
                         ON deal.Deal_ID = ol.dealid
               INNER JOIN [Atom.order_header] oh    
                  ON oh.orderid = ol.orderid
               INNER JOIN  (SELECT customerId as customerId 
                                   ,orderSummary.firstPurchaseDetail.orderId as First_Order_ID  
                                   , orderSummary.firstPurchaseDetail.purchaseDate as First_Purchase_Date
                                   , orderSummary.firstPurchaseDetail.purchaseDate - orderSummary.firstPurchaseDetail.purchaseDate % 86400000 - 19800000 as Round_First_Purchase_Date
                            FROM [Atom.customer]) cust
                  ON oh.customerid = cust.customerId
          WHERE oh.ispaid = 't'
            AND ol.ispaid = 't'
            AND oh.source is not null
                   and oh.source <> 'mobile'      
          GROUP BY Order_ID, Order_CreatedAt, Order_CreatedAt_WeekDate, Category, is_New_Customer, Customer_ID
) base
GROUP BY  Order_CreatedAt_WeekDate,  Category, is_New_Customer, Customer_ID
) base2
WHERE base2.is_New_Customer = 0
GROUP BY Order_CreatedAt_WeekDate, Category
) base3 ON dt.Week_Start_Epoch_IST = base3.Order_CreatedAt_WeekDate
WHERE DATE(dt.DATE) < CURRENT_DATE()
-- WHERE DATE(dt.DATE) < CASE WHEN DAYOFWEEK(CURRENT_DATE()) = 1 THEN DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), -6, 'Day'), 1))) 
--                                                                     ELSE DATE(TIMESTAMP(UTC_USEC_TO_WEEK(DATE_ADD(CURRENT_DATE(), 0, 'Day'), 1))) 
--                                                                 END -- before_Date
GROUP BY -- Date, 
         Week_Start_Date, Week_End_Date, Category, Metric_Value
) exist_cust

WHERE Week_Start_Date >= DATE('2015-12-20')
ORDER BY Week_Start_Date desc, METRIC_Name, Category